const redCards = (matches, players, year) => {
  const teamsInParticularYear = matches.filter((match) => match.Year === year);
  const matchID = teamsInParticularYear.map((team) => team.MatchID); //get only MatchID
  const compareMatchID = players.filter(
    (player) => matchID.some((id) => id === player.MatchID) //compare for the MatchID's
  );
  let filterRedCards = compareMatchID.filter((id) => id.Event.includes("R")); //See which teams have red cards
  let teamInitials = filterRedCards.map((card) => card["Team Initials"]); //for key values
  let totalRedCards = teamInitials.reduce((acc, card) => {
    if (!acc[card]) {
      //reduce to object with key is Team Initials and count of red card as value
      acc[card] = 1;
    } else {
      acc[card]++;
    }
    return acc;
  }, {});
  console.log(totalRedCards);
  return totalRedCards;
};

module.exports = redCards;
