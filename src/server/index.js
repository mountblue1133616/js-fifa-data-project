//CONSTANTS
const fs = require("fs");
const csv = require(`csvtojson`);
const matchesPerCity = require(`../server/1-matches-per-year.cjs`);
const matchesWonPerTeam = require(`../server/2-matches-won-per-team-per-year.cjs`);
const redCards = require(`../server/3-red-cards-per-team.cjs`);
const topGoals = require(`../server/4-top10-goals.cjs`);

//PATHS
const matchesPath = `../data/WorldCupMatches.csv`;
const playersPath = `../data/WorldCupPlayers.csv`;

//READING AND WRITING DATA

const forMatchesInCity = (matchesPath) => {
  csv()
    .fromFile(matchesPath)
    .then((matches) => {
      let matchesPerCityCall = matchesPerCity(matches);
      fs.writeFile(
        "../public/output/1-matches-per-year.json",
        JSON.stringify(matchesPerCityCall),
        (err) => {
          if (err) {
            console.log(err);
          }
        }
      );
    });
};

const forMatchesWon = (matchesPath) => {
  csv()
    .fromFile(matchesPath)
    .then((matches) => {
      let matchesWonPerTeamCall = matchesWonPerTeam(matches);
      fs.writeFile(
        "../public/output/2-matches-won-per-team-per-team.json",
        JSON.stringify(matchesWonPerTeamCall),
        (err) => {
          if (err) {
            console.log(err);
          }
        }
      );
    });
};

const forRedCards = (matchesPath, playersPath, year) => {
  csv()
    .fromFile(matchesPath)
    .then((matches) => {
      csv()
        .fromFile(playersPath)
        .then((players) => {
          let redCardsCall = redCards(matches, players, year);
          fs.writeFile(
            `../public/output/3-red-cards-per-team.json`,
            JSON.stringify(redCardsCall),
            (err) => {
              if (err) {
                console.log(err);
              }
            }
          );
        });
    });
};

const forTopGoals = (playersPath) => {
  csv()
    .fromFile(playersPath)
    .then((players) => {
      let topGoalsCall = topGoals(players);
      fs.writeFile(
        `../public/output/4-top-10-goals.json`,
        JSON.stringify(topGoalsCall),
        (err) => {
          if (err) {
            console.log(err);
          }
        }
      );
    });
};

//CALLING FUNCTIONS TO READ DATA
forMatchesInCity(matchesPath);
forMatchesWon(matchesPath);
forRedCards(matchesPath, playersPath, "2014");
forTopGoals(playersPath);
