const matchesWonPerTeam = (matches) => {
  const winnerCount = matches.reduce((acc, match) => {
    if (match["Home Team Goals"] > match["Away Team Goals"]) {
      if (acc[match["Home Team Name"]]) {
        acc[match["Home Team Name"]] += 1;
      } else acc[match["Home Team Name"]] = 1;
    } else if (match["Away Team Goals"] > match["Home Team Goals"]) {
      if (acc[match["Away Team Name"]]) {
        acc[match["Away Team Name"]] += 1;
      } else acc[match["Away Team Name"]] = 1;
    }
    return acc;
  }, {});
  // console.log(winnerCount);
  return winnerCount;
};

module.exports = matchesWonPerTeam;
