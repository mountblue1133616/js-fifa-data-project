const topGoals = (players, numberOfTopPlayers = 10) => {
  const playerWithGoal = players.filter((player) => player.Event.includes("G")); //filter out the array which includes G

  const goalCountOfPlayer = playerWithGoal.reduce((acc, player) => {
    let eventArray = player.Event.split(" "); //split with space for OG and G
    const goalEvents = eventArray.filter((event) => event.startsWith("G"));
    const ownGoalEvents = eventArray.filter((event) => event.startsWith("OG"));

    const goals = goalEvents.length - ownGoalEvents.length;

    if (acc[player["Player Name"]]) {
      acc[player["Player Name"]] += goals; // key is Player Name and value is goals
    } else {
      acc[player["Player Name"]] = goals;
    }
    return acc;
  }, {});

  const matches = players.reduce((acc, player) => {
    // matches a particular player has played
    if (!acc[player["Player Name"]]) {
      acc[player["Player Name"]] = 1;
    } else {
      acc[player["Player Name"]]++;
    }
    return acc;
  }, {});
  // console.log(matches);

  let comparisionOfPLayers = Object.keys(matches).reduce((acc, player) => {
    //compare the keys for both the object
    if (player in goalCountOfPlayer) {
      acc[player] = goalCountOfPlayer[player] / matches[player];
    }
    return acc;
  }, {});

  // console.log(comparision);

  const sortedPlayers = Object.entries(comparisionOfPLayers) // convert in array of key-value pairs
    .sort((a, b) => b[1] - a[1]) //sort in descending
    .slice(0, numberOfTopPlayers); // for top 10

  const topPlayers = Object.fromEntries(sortedPlayers); // as probailty cannot be greater we takeout for base 4 i.e the highest value
  const probabiltyForGoal = Object.keys(topPlayers).reduce((acc, key) => {
    acc[key] = topPlayers[key] / topPlayers[Object.keys(topPlayers)[0]];
    return acc;
  }, {});
  console.log(probabiltyForGoal);
  return probabiltyForGoal;
};

module.exports = topGoals;
