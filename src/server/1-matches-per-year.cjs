const matchesPerCity = (matches) => {
  const cities = matches.map((match) => match.City);
  let findMatchPerCity = cities.reduce((acc, city) => {
    //reduce the mapped array to object with count of matches.Key will store city name and value would be the count
    if (!acc[city]) {
      acc[city] = 1;
    } else {
      acc[city]++;
    }
    return acc;
  }, {});
  return findMatchPerCity;
};

module.exports = matchesPerCity;
